import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import homePage from './homePage';
import menuClik from './Component/menuclik/MenuClik';
import homeData from './Component/Data/homeData';
import DataId from './Component/Data/getDataId/DataID';



class AppRouter extends Component{
    render(){
        return(
        <div>
            <BrowserRouter>
                <div>
                    <Route exact path='/' component={homePage} />

                    <Route path='/menuclik' component={menuClik} />
                    <Route path='/data' component={homeData} />
                    <Route path="/dataid/:id" component={DataId} />

             
                  
                </div>
            </BrowserRouter>
        </div>
        )
    }
}

export default AppRouter;