import React from 'react';
import {Container,Row} from 'reactstrap';
import { CardImg, CardText, CardBody,
  CardTitle, Button } from 'reactstrap';



const GetData = (props) =>{
    return (
      <div>
        <Container>
            <Row>
            <div className='produkcard'key={props.id}>
            <CardImg className="imgbos" top width="100%" src={props.data.categories}alt="Card image cap" />
            <CardBody>
              <CardTitle   >{props.data.title}</CardTitle>
              <CardText className="contentCArd">{props.data.content}</CardText>
              <Button className="button" onClick={()=> props.remove(props.data.id)} >Remove</Button>
              <Button className="button" onClick={()=>props.edit(props.data)}>Edit</Button>
              <Button className="button" onClick={()=>props.dataid(props.data.id)} >Detail</Button>
            </CardBody>
            </div>
            </Row>
        </Container>
       
      </div>
    )
  }
export default GetData;
