import React, { Component } from 'react';
import Axios from 'axios';
import Appheader from '../../AppHeader';
import {Container} from 'reactstrap'


export default class DataId extends Component {
    state={
        produkpost:{
            title:"",
            categories:"",
            content:""
        }
    }
    componentDidMount(){
      
        let idPost= this.props.match.params.id;
       Axios.get(`http://reduxblog.herokuapp.com/api/posts/${idPost}`)
        .then(res =>{
            console.log('res', res)
            let post = res.data;
            this.setState({
                produkpost:{
                    title: post.title,
                    categories: post.categories,
                    content: post.content
                }
            })
        })
    }
    render() {
        return ( 
            <div>
                <Appheader />
                <Container>
                    <div className="produkcarddetail">
                        <img className="gambar" src={this.state.produkpost.categories} alt="gambar" />
                        <div className="title">{this.state.produkpost.title}</div>
                        <div className="content">{this.state.produkpost.content}</div>
                    </div>
                </Container>
            </div>
        )
    }
}
