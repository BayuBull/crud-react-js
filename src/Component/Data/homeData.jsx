import React, { Component } from 'react';
import {Container} from 'reactstrap';
import Appheader from '../AppHeader';
import GetData from './getData';
import Axios from 'axios';
import { Button, Form, FormGroup, Label, Input} from 'reactstrap';

export default class homeData extends Component {
    constructor(){
        super();
           this.state={
                produk:[],
                produkpost:{
                    id:"",
                    title:"",
                    categories:"",
                    content:""
                },
                isEdit:false
              }
            }
            // post
            addApiPost = () =>{
              Axios.post('http://reduxblog.herokuapp.com/api/posts?key=didik',this.state.produkpost)
              .then((res)=>{
                  console.log("respon_post",res)
                  this.setState({
                    produkpost:{
                      id:"",
                      title:"",
                      categories:"",
                      content:""
                  }
                  })
                  window.location.reload();
              },(err)=>{
                console.log("err",err)
              })
            }
            handleChange = (event) => {
                let produkpostNew = {...this.state.produkpost};
                let timeID = new Date().getTime();
                if(!this.state.isEdit){
                  produkpostNew['id'] = timeID;
                }
                produkpostNew[event.target.name]=event.target.value;
                this.setState({
                  produkpost: produkpostNew
                })
            } 
            handleSubmit=()=>{
              if(this.state.isEdit){
                  this.putToApi();
              }else{
                this.addApiPost();
              }
            } 
            //--<
            //get api jadiin fungsi ->
            getToapi=(data)=>{
              Axios.get('http://reduxblog.herokuapp.com/api/posts?key=didik')
              .then((res) => {
                  this.setState({
                      produk:res.data
                      
                  })
              })
            }
            //---<

            //get data ->
            componentDidMount(){
               this.getToapi();
            }
            //---<

            //remove data -->
            handleRemove(data){
              console.log(data)
              Axios.delete(`http://reduxblog.herokuapp.com/api/posts/${data}`)
                .then((res) => {
                  // console.log(res)
                    this.getToapi();
                    
                    })
            }
            //---<
            // Edit by ID (fungsi tidak berjalan karean tidak ada put method di redux blog)
            putToApi=()=>{
              Axios.put(`http://reduxblog.herokuapp.com/api/posts/${this.state.produkpost.id}`,this.state.produkpost)
              .then((res)=>{
                this.getToapi();
            },(err)=>{
              console.log("err",err)
            })
          }

            handleEdit = (data) => {
              console.log(data);
              this.setState({
                produkpost: data,
                isEdit : true,
              })   
            }
            handleID = (id) =>{
              this.props.history.push(`/dataid/${id}`);
            }
  render() {
      // console.log(this.state.produk)
    return (
      <div>
        <Appheader />
        <Container>
          <div className="backgroundImput">
              <div className="BoxInput">
                <Form>
                <FormGroup>
                  <Label for="title" >Title</Label>
                  <Input type="textarea" value={this.state.produkpost.title} name="title" id="title" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                  <Label for="content">Content</Label>
                  <Input type="textarea" value={this.state.produkpost.content} name="content" id="content" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                  <Label for="categories">Image</Label>
                  <Input type="textarea" value={this.state.produkpost.categories} name="categories" id="categories" placeholder="hanya link img" onChange={this.handleChange} />
                </FormGroup>
                <Button onClick={this.handleSubmit}>Submit</Button>
                </Form>
              </div>
          </div>
        <div className="layoutContent">
            {
            this.state.produk.map( produk => {
                return (  
                <GetData key= {produk.id} data={produk} remove={this.handleRemove} edit={this.handleEdit} dataid={this.handleID}/>
                )   
             })
            }
            </div>
        </Container>
      </div>
    )
  }
}
