import React, {Component} from 'react';
import Appheader from '../AppHeader';
import {Container,Row} from 'reactstrap';
import MenuClikSatu from './MenuClikSatu';

  



class menuClik extends Component{
   state ={
       angka:0
   }
handlemerubahNilaiDariChild = (valueClid) =>{
    this.setState({
        angka:valueClid
    })
}
    render(){
        return(
            <div>
            <Appheader />
                <Container>
                        <Row>
                            <div className="Judul">
                                <h4>Passing State Clid To Perent Component dengan FungsionaComponen</h4>
                            </div>
                            <div className="backClik">
                                <div className="kartuAtas"> 
                                    <div className="penghitungAngkaAtas"><h3>{this.state.angka}</h3></div>
                                </div>
                                <MenuClikSatu merubahNilaiDariChild={(newValue)=> this.handlemerubahNilaiDariChild(newValue)} />
                            </div>
                        </Row>
                </Container>
            </div>
        )
    }
}
export default menuClik;