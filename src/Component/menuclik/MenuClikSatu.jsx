import React, {Component} from 'react';
import {Container,Row,Button} from 'reactstrap';

  



class menuClikSatu extends Component{
   state ={
       angka:0
   }
   handleKirimKePerent = (newValue) =>{
       this.props.merubahNilaiDariChild(newValue);
   }
   handlePlus =() =>{
    this.setState({
        angka: this.state.angka + 1
    }, ()=>{
        this.handleKirimKePerent (this.state.angka);
    }) 
   }

   handleMinus =() =>{
       if(this.state.angka > 0){
        this.setState({
            angka: this.state.angka - 1
        }, ()=>{
            this.handleKirimKePerent(this.state.angka)
        })
       }
  
   }
    render(){
        return(
            <div>
                <Container>
                        <Row>
                        <div className="kartuUtama"> 
                                    <div className="judulKartu"> Penghitung Clik</div>
                                        <div className="penghitungAngka"><h3>{this.state.angka}</h3></div>
                                        <Button type="button" className="btn btn-primary" onClick={this.handlePlus}>Tambah</Button>
                                        <Button type="button" className="btn btn-primary" onClick={this.handleMinus}>Kurang</Button>
                                </div>      
                        </Row>
                </Container>
            </div>
        )
    }
}
export default menuClikSatu;