import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    Button
    } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';


class Appheader extends Component{
     constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <div className="bodyNav">
        <Navbar light expand="md">
          <NavbarBrand href="/">Latihan React</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to='/'><Button className="ButtonNav">Home</Button></Link>
              </NavItem>
              <NavItem>
             <Link to='/menuclik'><Button className="ButtonNav">Chil to Parent</Button></Link>
              </NavItem>
              <NavItem>
             <Link to='/data'><Button className="ButtonNav">Data</Button></Link>
              </NavItem>                
            </Nav>
          </Collapse>
        </Navbar>
        </div>
      </div>
    );
  }
}

export default Appheader;